// Konzipieren Sie einen effizienten Algorithmus, der für einen gegebenen Wert n > 1 den größten
// echten Teiler z bestimmt. Für echte Teiler gilt: n % z == 0 und n != z
// Implementieren Sie eine Methode int greatestProperDivisor( int n ), die den Algo-
// rithmus umsetzt.
// 
// Hinweis: Für Primzahlen ist der größte echte Teiler immer 1.

class Aufgabe2 {
    public static void main(String[] args) {

        for (int i = 1; i < 9999; i++) {

            System.out.println( "Gröster teiler der" + i  +"  ist: " + greatestProperDivisor(i));
        }

        System.out.println("");
    }

    private static int greatestProperDivisor(int a) {

        if(a <= 1) {
            return a;
        }

        int divisor = a / 2;
        
        while( a % divisor != 0) {
            divisor = divisor -1;
        }

        return divisor;

    }
}