package game_of_life;

import java.util.Arrays;
/**
 * Write a description of class GameOfLife here.
 * 
 * @author David Mezlaf 
 * @version none
 */
public class GameOfLife
{
    private boolean[][] cells;
    private int gen = 0;

    /**
     * Legt ein neues Objekt der Klasse GameOfLife an
     * und initialisiert das Attribut cells
     * als quadratisches Feld mit Länge n,
     * sodass jeder Eintrag zunächst den Wert false hat.
     */
    public GameOfLife(int n) {
        cells = new boolean[n][n];
    }

    /**
     * Legt ein neues Objekt der Klasse GameOfLife an
     * und initialisiert das Attribut cells
     * mit den Inhalten des übergebenen Feldes.
     */
    public GameOfLife(boolean[][] cells) {
        for (int i = 0; i < cells.length; i++) { // cells muss Rechteckig sein.
            for (int k = 0; k < cells[i].length; k++) {
                if (cells[i].length != cells.length) {
                    throw new RuntimeException();
                }
            }
        }
        this.cells = cells;
    }
    
    /**
     * Zu implementieren!
     * 
     * Berechnet in der durch cells repräsentierten Population
     * den Zustand (lebendig oder tot) der Zelle an Position (i, j).
     */
    public boolean nextState(int i, int j)
    {
        int sum = this.getSumOfNeigbours(i, j);

        if(cells[i][j]) {
            return sum >= 2 && sum < 4;
        } 
        return sum == 3;
    }
    
    /**
     * Zu implementieren!
     * 
     * Setzt das Feldattribut cells auf die direkte Nachfolgepopulation
     * der durch cells aktuell repräsentierten Population.
     */
    public void nextGeneration()
    {
        boolean[][] newCells = new boolean[cells.length][cells.length];

        for (int i = 0; i < cells.length; i++) { 
            for (int j = 0; j < cells[i].length; j++) {

                newCells[i][j] = this.nextState(i, j);
            }
        }

        this.cells = newCells;
        this.gen ++;
        
    }
    
    /**
     * Zu implementieren!
     * 
     * Setzt das Feldattribut cells auf die n-te Nachfolgepopulation
     * der durch cells aktuell repräsentierten Population.
     */
    public void futureGeneration(int n)
    {
        ;
    }
    
    /**
     * Zu implementieren! 
     * 
     * Gibt die Inhalte des Feldattributs cells 
     * anschaulich in der Konsole aus.
     * Tipp: Verwende die Methode printCell für
     * die Ausgabe einer Zelle. 
     * Diese repräsentiert eine Zelle durch
     * zwei Zeichen: "X " (lebend) oder     "  " (tot)
     */
    public void show()
    {
        System.out.print("\033[H\033[2J");  
        System.out.flush();

        //String hline = "---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------";
        //String vline = "|";


        
        for (int i = 0; i < cells.length; i++) {
            System.out.println();
            //System.out.println(hline);
            //System.out.print(vline);
            for (int j = 0; j < cells[i].length; j++) {

                //System.out.print(vline);
                this.printCell(i, j);
            }
        }
        System.out.println();
        //System.out.println(hline);

    }
    
    /**
     * Hilfsfunktion. Kann genutzt werden, 
     * um sich im Zeitabstand von n Millisekunden
     * die aufeinanderfolgenden Populationen in der Konsole
     * zeichnen zu lassen.
     */
    public void run(int n)
    {
        while(gen < 54){
            nextGeneration();
            show();
            wait(n);
        }
    }
    
    /**
     * Hilfsfunktion; Repräsentiert eine lebende Zelle durch "X "
     * und eine tote Zelle durch "  " (zwei Leerzeichen)
     * und gibt diese Repräsentation in der Konsole aus.
     */
    private void printCell(int i, int j)
    {
        if(cells[i][j])
        {
            System.out.format("%1s ", "■");
        } 
        else
        {
            System.out.format("%1s ", "");
        }
    }

    /**
     * Hilfsfunktion; zwingt das laufende Programm,
     * n Millisekunden zu warten. Wird in run(int n) verwendet.
     */
    private void wait(int millis) {

        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    /**
     * Hilfsfunktion; setzt cells auf eine zufällige Population, 
     * in der jede Zelle mit einer Wahrscheinlichkeit von etwa 0.3 lebt.
     */
    public void randomState() {
        for (boolean[] arr : cells) {
            for (int i = 0; i < arr.length; i++) {
                arr[i] = java.lang.Math.random() < 0.3;
            }
        }
    }


    private int getSumOfNeigbours(int i, int j) {
        int sum = 0;

        sum = sum + getValueOfCell(i -1, j -1); 
        sum = sum + getValueOfCell(i -1, j   );
        sum = sum + getValueOfCell(i -1, j +1);
        sum = sum + getValueOfCell(i   , j -1);
        sum = sum + getValueOfCell(i   , j +1);
        sum = sum + getValueOfCell(i +1, j -1);
        sum = sum + getValueOfCell(i +1, j   );
        sum = sum + getValueOfCell(i +1, j +1);

        return sum;
    }

    private int getValueOfCell(int i, int j) {
        try {
            
            return this.cells[i][j] ? 1: 0;

        } catch(IndexOutOfBoundsException exception) {
            return 0;
        }
    }

    public void setglider() {

        cells[0][1] = true;
        cells[1][2] = true;
        cells[2][0] = true;
        cells[2][1] = true;
        cells[2][2] = true;

    }

    public void set_r_Pentomino() {

        int off = this.cells.length / 2;

        cells[off -1][off -1] = true;
        cells[off -1][off -2] = true;
        cells[off -1][off -3] = true;
        cells[off -1][off +1] = true;
        cells[off -1][off +2] = true;
        cells[off -1][off +3] = true;


        cells[off][off -3] = true;
        cells[off][off +3] = true;

        cells[off +1][off -3] = true;
        cells[off +1][off -2] = true;
        cells[off +1][off -1] = true;

        cells[off +1][off +3] = true;
        cells[off +1][off +2] = true;
        cells[off +1][off +1] = true;




        
    }
}
