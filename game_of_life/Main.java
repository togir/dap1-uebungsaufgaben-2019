package game_of_life;

/**
 * Dient der Anwendung der Klasse GameOfLife. Kann genutzt werden, um sich im
 * Zeitabstand von 2 Sekunden die aufeinanderfolgenden Populationen in der
 * Konsole zeichnen zu lassen.
 * 
 * @author (your name)
 * @version (a version number or a date)
 */
public class Main
{
    public static void main(String[] args)
    {

        
        while (true) {
            GameOfLife gol = new GameOfLife(40);
            //gol.randomState();
            gol.set_r_Pentomino();
            gol.run(10);
        }
    }
}
