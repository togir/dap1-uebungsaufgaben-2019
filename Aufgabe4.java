// Auf dem Präsenzblatt 2 haben Sie bereits in Aufgabe 3 verschiedene Algorithmen konzipiert,
// die in dieser Aufgabe nun implementiert werden sollen.
// Gehen sie in dieser Aufgabe davon aus, dass Ihnen für jeden der beiden Klausurstapel ein
// (unsortiertes) Array der dazugehörigen Matrikelnummern zur Verfügung steht.
// Implementieren Sie folgende Methoden:

// • boolean writtenBoth ( int[] exam1, int[] exam2 )
// soll true zurückgeben, falls es mindestens eine Matrikelnummer gibt, die sowohl in exam1 als auch in exam2
// vorkommt. sonst soll false zurückgegeben werden.

// • boolean notWrittenBoth ( int[] exam1, int[] exam2 ) soll true zurück-
// geben, wenn es keinen Studierenden gibt, der biede Klausuren mitgeschrieben hat. Ansonsten soll false zurückgegeben werden.

// • int countWrittenBoth ( int[] exam1, int[] exam2 ) soll die Anzahl an glei-
// chen Matrikelnummern in exam1 und exam2 ermitteln.
// • boolean allNrBigger ( int[] exam1, int[] exam2 ) soll true zurückgeben,
// wenn alle Matrikelnummer in exam1 größer sind als in exam2, sonst false.

// Zustazaufgabe: Implementieren Sie die obigen Methoden unter der Annahme, dass die Array
// stets aufsteigend sortiert sind.

class Aufgabe4 {
    public static void main(String[] args) {
        
    }

    private static boolean writtenBoth(int[] exam1, int[] exam2) {

    }

    private static boolean notWrittenBoth(int[] exam1, int[] exam2) {

    }

    private static int countWrittenBoth(int[] exam1, int[] exam2) {

    }

    private static boolean allNrBigger(int[] exam1, int exam2) {

    }

    private static int[] sortArray(int[] arr) {
        HeapSort.heapSort(arr);
        return arr;
    }

    private static int[] generateArray(int maxEntrys) {

        int[] arr = new int[Math.abs((int) Math.random() % maxEntrys)];
        
        for (int i = 0; i < arr.length ; i++) {
            arr[i] = ;
        }
    }

}