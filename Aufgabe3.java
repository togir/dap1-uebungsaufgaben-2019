// Implementieren Sie eine Methode int countPairs( int[] arr ), die die Anzahl der Paa-
// re von zwei unmittelbar aufeinander folgenden gleichen Werten in einem Feld bestimmt. Dabei
// soll jedes Element des Feldes nur zu genau einem Paar gezählt werden.

class Aufgabe3 {
    public static void main(String[] args) {

        int[] arr1 = {1, 1, 3, 3, 1, 2, 2, 2, 1};
        int[] arr2 = {1, 3, 3, 3, 3, 2, 2, 5, 5};

        System.out.println("Array 1 besitzt " + countPairs(arr1) + " Paare.");
        System.out.println("Array 2 " + countPairs(arr2) + " Paare");
        
        
    }

    public static int countPairs(int[] arr)
    {
        int count = 0;
        for ( int i = 1; i < arr.length; i++ )
        {
            if(arr[i-1] == arr[i]) {
                count ++;
                i++;
            }
        }
        return count;
    }

    
}